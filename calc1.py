import operator
# Token types
#
# EOF (end-of-file) token is used to indicate that
# there is no more input left for lexical analysis
INTEGER, PLUS, EOF, MINUS, MULTIPLY, DIVIDE, PAREN_RIGHT,PAREN_LEFT = 'INTEGER', 'PLUS', 'EOF', 'MINUS' , 'MULTIPLY' , 'DIVIDE' , ')','('


class Token(object):
    def __init__(self, type = None, value = None):
        # token type: INTEGER, PLUS, or EOF
        self.type = type
        # token value: 0, 1, 2. 3, 4, 5, 6, 7, 8, 9, '+', or None
        self.value = value

    def __str__(self):
        """String representation of the class instance.

        Examples:
            Token(INTEGER, 3)
            Token(PLUS '+')
        """
        return 'Token({type}, {value})'.format(
            type=self.type,
            value=repr(self.value)
        )

    def __repr__(self):
        return self.__str__()


class Interpreter(object):
    def __init__(self, text):
        # client string input, e.g. "3+5"
        self.text = text.replace(" ", "")
        self.text.replace(" ", "")
        # self.pos is an index into self.text
        self.pos = 0
        # current token instance
        self.current_token = None

    def error(self):
        raise Exception('Error parsing input')




    def is_char_an_op(current_char,self):
        if current_char == '+' or '-' or '*' or '/' or '(' or ')':
            return True

    def get_next_token(self):
        """Lexical analyzer (also known as scanner or tokenizer)

        This method is responsible for breaking a sentence
        apart into tokens. One token at a time.
        """
        op_to_word = {'+':PLUS, '-':MINUS, '*':MULTIPLY , '/':DIVIDE }
        text = self.text

        # is self.pos index past the end of the self.text ?
        # if so, then return EOF token because there is no more
        # input left to convert into tokens
        if self.pos > len(text) - 1:
            return Token(EOF, None)

        # get a character at the position self.pos and decide
        # what token to create based on the single character
        current_char = text[self.pos]

        # if the character is a digit then convert it to
        # integer, create an INTEGER token, increment self.pos
        # index to point to the next character after the digit,
        # and return the INTEGER token

        if current_char.isdigit():
            holder = ''
            for char in self.text[self.pos:]:
                if not char.isdigit():
                    break
                else:
                    holder +=  char
                    self.pos += 1
            token = Token(INTEGER, int(holder))

            return token

        if self.is_char_an_op(current_char):
            token = Token(op_to_word[current_char], current_char)
            self.pos += 1
            return token

        self.error()


    def operate_on_integers(left, right, op):
        operations = {PLUS: operator.add , MINUS: operator.sub , MULTIPLY: operator.mul, DIVIDE: operator.truediv}
        return operations[op](left,right)

    def master_calculate(tokens):
        Interpreter.calculate(DIVIDE,tokens)
        Interpreter.calculate(MULTIPLY,tokens)
        Interpreter.calculate(PLUS,tokens)
        Interpreter.calculate(MINUS,tokens)
        return tokens


    def calculate(operation,tokens):
        counter = 0
        for token in tokens:
            if token.type == operation:
                counter+=1

        while counter != 0:
            print(tokens)
            for i in range(len(tokens)):
                if tokens[i].type == operation:
                    counter -= 1
                    total = Interpreter.operate_on_integers(tokens[i-1].value,tokens[i+1].value,operation)
                    tokens[i] = Token(INTEGER,total)
                    del tokens[i+1]
                    del tokens[i-1]
                    break


    def expr(self):
        def Generate_ordering_for_calculation():
            self.current_token = self.get_next_token()
            tokens = [self.current_token]
            while self.current_token.value != None:
                    self.current_token = self.get_next_token()
                    tokens.append(self.current_token)
            tokens.pop()
            return tokens





        # after the above call the self.current_token is set to
        # EOF token
        tokens = Generate_ordering_for_calculation()
        answer = Interpreter.master_calculate(tokens)


        return answer[0].value


def main():
    while True:
        try:
            text = input('calc> ')
        except EOFError:
            break
        if not text:
            continue
        interpreter = Interpreter(text)
        result = interpreter.expr()
        print(result)


if __name__ == '__main__':
    main()
